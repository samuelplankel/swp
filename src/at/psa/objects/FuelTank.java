package at.psa.objects;

public class FuelTank {
    private int tankSize;
    private int fuelAmount;

    public FuelTank(int tankSize, int fuelAmount) {
        this.tankSize = tankSize;
        this.fuelAmount = fuelAmount;
    }
}
