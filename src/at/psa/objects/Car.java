package at.psa.objects;

import java.util.ArrayList;
import java.util.List;

public class Car {
    private int fuelConsumption;
    private int fuelAmount;
    private Engine engine;
    private FuelTank fuelTank;
    private List<RearMirror> mirrors;
    private List<Tire> tires;
    private String brand;
    private String serialNumber;
    private String color;

    public Car(Engine engine, FuelTank fuelTank, int fuelConsumption, String brand, String serialNumber) {
        this.engine = engine;
        this.fuelConsumption = fuelConsumption;
        this.brand = brand;
        this.serialNumber = serialNumber;
        this.mirrors = new ArrayList<>();
        this.tires = new ArrayList<>();
    }

    public void addMirror(RearMirror rearMirror) {
        this.mirrors.add(rearMirror);
    }

    public List<RearMirror> getMirrors() {
        return mirrors;
    }

    public void addTire(Tire tire) {
        this.tires.add(tire);
    }

    public List<Tire> getTires() {
        return tires;
    }

    public void drive(int amount) {
        this.engine.drive(amount);
    };

    public void breaking() {
        System.out.println("I am breaking");
    };

    public void turboBoost() {
        if (this.fuelAmount >= (this.fuelAmount/10)) {
            System.out.println("SuperBoostMode");
        } else {
            System.out.println("Not enough fuel to go to SuperBoostMode");
        }
    }

    public void honk(int amountOfRepetitions) {
        for(int i=0;i<=amountOfRepetitions;i++) {
            System.out.println("Tuuut");
        }
    }

    public void getRemainingRange() {
        System.out.println("You can drive " + this.fuelAmount/this.fuelConsumption + " times");
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }
}
