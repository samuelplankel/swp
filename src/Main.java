import at.psa.objects.*;

public class Main {
    public static void main(String[] args) {
        Engine e1 = new Engine( 140, Engine.TYPE.DIESEL);
        FuelTank ft1 = new FuelTank(100,100);
        RearMirror r1 = new RearMirror(100, 0);
        RearMirror r2 = new RearMirror(90,-10);
        Tire t1 = new Tire(17, 40);
        Tire t2 = new Tire(17, 40);
        Tire t3 = new Tire(17, 40);
        Tire t4 = new Tire(17, 40);
        Car c1 = new Car(e1,ft1,8,"opel","12333");
        c1.addMirror(r1);
        c1.addMirror(r2);
        c1.addTire(t1);
        c1.addTire(t2);
        c1.addTire(t3);
        c1.addTire(t4);

        c1.drive(77);
    }
}

