import java.time.Clock;
import java.util.Scanner;

public class tictactoe {

    public static void main(String[] args) {
        char[][] field = {{'|',' ', '|', ' ', '|', ' ', '|'},
                {'|',' ', '|', ' ', '|', ' ', '|'},
                {'|',' ', '|', ' ', '|', ' ', '|'}};

        boolean gameStatus = true;
        int player = 1;

        while(gameStatus == true) {
            Scanner scan = new Scanner(System.in);

            System.out.println("Spieler " + player + " gib ein feld an:");
            String input = scan.next();
            String[] in = input.split(",");
            int x = Integer.parseInt(in[0]);
            int y = Integer.parseInt(in[1]) * 2 + 1;

            if(field[x][y] == ' ') {
                if(player == 1) {
                    field[x][y] = 'x';
                    player = 2;
                } else {
                    field[x][y] = 'o';
                    player = 1;
                }
            } else {
                System.out.println("Wähle ein anderes Feld");
            }

            printField(field);
            checkWin(field);
        }

    }

    public static void playAgain(char[][] field) {
        System.out.println("If you want to play again type '1'");
        Scanner scan = new Scanner(System.in);
        String input = scan.next();
        int in = Integer.parseInt(input);
        System.out.println(in);

        if(in == 1) {
            for(int x=0;x<3;x++) {
                for(int j=0;j<7;j++) {
                    if((field[x][j] == 'x') || (field[x][j] == 'o')) {
                        field[x][j] = ' ';
                    }
                }
            }
        }
    }

    public static void checkWin(char[][] field) {
        if((field[0][1] == 'x' && field[0][3] == 'x' && field[0][5] == 'x') ||
            (field[0][1] == 'x' && field[1][3] == 'x' && field[2][5] == 'x') ||
            (field[0][1] == 'x' && field[1][1] == 'x' && field[2][1] == 'x') ||
            (field[1][1] == 'x' && field[1][3] == 'x' && field[1][5] == 'x') ||
            (field[0][3] == 'x' && field[1][3] == 'x' && field[2][3] == 'x') ||
            (field[2][1] == 'x' && field[2][3] == 'x' && field[2][5] == 'x') ||
            (field[0][5] == 'x' && field[1][5] == 'x' && field[2][5] == 'x') ||
            (field[0][5] == 'x' && field[1][3] == 'x' && field[2][1] == 'x'))
        {
            System.out.println("Player 1 won!");
            playAgain(field);

        } else if ((field[0][1] == 'o' && field[0][3] == 'o' && field[0][5] == 'o') ||
                (field[0][1] == 'o' && field[1][3] == 'o' && field[2][5] == 'o') ||
                (field[0][1] == 'o' && field[1][1] == 'o' && field[2][1] == 'o') ||
                (field[1][1] == 'o' && field[1][3] == 'o' && field[1][5] == 'o') ||
                (field[0][3] == 'o' && field[1][3] == 'o' && field[2][3] == 'o') ||
                (field[2][1] == 'o' && field[2][3] == 'o' && field[2][5] == 'o') ||
                (field[0][5] == 'o' && field[1][5] == 'o' && field[2][5] == 'o') ||
                (field[0][5] == 'o' && field[1][3] == 'o' && field[2][1] == 'o'))
        {
            System.out.println("Player 2 won!");
            playAgain(field);
        }
    }

    public static void printField(char[][] field) {
        for(char[] row : field) {
            for(char c : row) {
                System.out.print(c);
            }
            System.out.println();
        }
    }
}
